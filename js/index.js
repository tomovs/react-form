// Component for input
var inputGroupClasses;
var cx = classNames.bind(inputGroupClasses);

var Input = React.createClass({
  getInitialState: function(){
    var valid = (this.props.isValid && this.props.isValid()) || true;

    return {
      valid: valid,
      empty: _.isEmpty(this.props.value),
      focus: false,
      value: null,
      iconsVisible: !this.props.validator,
      errorMessage: this.props.emptyMessage,
      validator: this.props.validator,
      validatorVisible: false,
      type: this.props.type,
      minCharacters: this.props.minCharacters,
      requireCapitals: this.props.requireCapitals,
      requireNumbers: this.props.requireNumbers,
      forbiddenWords: this.props.forbiddenWords,
      isValidatorValid: {
        minChars: false,
        capitalLetters: false,
        numbers: false,
        words: false,
        all: false
      },
      allValidatorValid: false
    };
  },

  handleChange: function(event){
    this.setState({
      value: event.target.value,
      empty: _.isEmpty(event.target.value)
    });

    if(this.props.validator) {
      this.checkRules(event.target.value)
    }

    // Input's validation method
    if(this.props.validate) {
      this.validateInput(event.target.value);
    }

    // onChange method on the parent component for updating it's state
    if(this.props.onChange) {
      this.props.onChange(event);
    }
  },

  validateInput: function (value) {
    // Validation method in the parent component
    if(this.props.validate && this.props.validate(value)){
      this.setState({
        valid: true,
        errorVisible: false
      });
    } else {
      this.setState({
        valid: false,
        errorMessage: !_.isEmpty(value) ? this.props.errorMessage : this.props.emptyMessage
      });  
    }

  },

  componentWillReceiveProps: function (newProps) {    
    // perform update only when new value exists and not empty  
    if(newProps.value) {
      if(!_.isUndefined(newProps.value) && newProps.value.length > 0) {
        if(this.props.validate) {
          this.validateInput(newProps.value);
        }
        this.setState({
          value: newProps.value,
          empty: _.isEmpty(newProps.value)
        });
      }   
    }
  },

  isValid: function () {
    if(this.props.validate) {
      if(_.isEmpty(this.state.value) || !this.props.validate(this.state.value)) {
        this.setState({
          valid: false,
          errorVisible: true
        });
      }
    }
   
    return this.state.valid;
  },

  handleFocus: function () {
    this.setState({
      focus: true,
      validatorVisible: true
    });

    // hide error when validator is active
    if(this.props.validator) {
      this.setState({
        errorVisible: false
      })
    }
  },

  handleBlur: function () {
    this.setState({
      focus: false,
      errorVisible: !this.state.valid,
      validatorVisible: false
    });
  },

  mouseEnterError: function () {
    this.setState({
      errorVisible: true
    });
  },

  hideError: function () {
    this.setState({
      errorVisible: false,
      validatorVisible: false
    });
  },

  // validator function
  checkRules: function(value) {
    var validData = {
      minChars: !_.isEmpty(value) ? value.length >= parseInt(this.state.minCharacters): false,
      capitalLetters: !_.isEmpty(value) ? this.countCapitals(value): false,
      numbers: !_.isEmpty(value) ? this.countNumbers(value) > 0 : false,
      words: !_.isEmpty(value) ? !this.checkWords(value) : false
    }
    var allValid = (validData.minChars && validData.capitalLetters && validData.numbers && validData.words);

    this.setState({
      isValidatorValid: validData,
      allValidatorValid: allValid,
      valid: allValid
    })
  },

  countCapitals: function(value) {
    var str = value;
    return str.replace(/[^A-Z]/g, "").length;
  },

  countNumbers: function(value) {
    return /\d/.test(value);
  },

  checkWords: function(value) {
    return  _.some(this.state.forbiddenWords, function (word) {
      var matched = (word === value) ? true : "";
      return matched
    })
  },

  render: function(){

    var inputGroupClasses = cx({
      'input_group':     true,
      'input_valid':     this.state.valid,
      'input_error':     !this.state.valid,
      'input_empty':     this.state.empty,
      'input_hasValue':  !this.state.empty,
      'input_focused':   this.state.focus,
      'input_unfocused': !this.state.focus
    });

    var validator;

    if(this.state.validator) {
      validator = 
        <PasswordValidator
          ref="passwordValidator"
          visible={this.state.validatorVisible}
          name={this.props.text}
          value={this.state.value}
          validData={this.state.isValidatorValid}
          valid={this.state.allValidatorValid}
          forbiddenWords={this.state.forbiddenWords}
          minCharacters={this.props.minCharacters}
          requireCapitals={this.props.requireCapitals}
          requireNumbers={this.props.requireNumbers}
        />
    }

    return (
      <div className={inputGroupClasses}>

        <label className="input_label" htmlFor={this.props.text}>
          <span className="label_text">{this.props.text}</span>
        </label>

        <input 
          {...this.props}
          placeholder={this.props.placeholder} 
          className="input" 
          id={this.props.text}
          defaultValue={this.props.defaultValue} 
          value={this.state.value} 
          onChange={this.handleChange} 
          onFocus={this.handleFocus}
          onBlur={this.handleBlur}
          autoComplete="off"
        />

        <InputError 
          visible={this.state.errorVisible} 
          errorMessage={this.state.errorMessage} 
        />

        <div className="validationIcons">
          <i className="input_error_icon" onMouseEnter={this.mouseEnterError}> <Icon type="circle_error"/> </i>
          <i className="input_valid_icon"> <Icon type="circle_tick"/> </i>
        </div>

        {validator}

      </div>
    );
  }
});

// Component for radio input
var Radio = React.createClass ({
  render() {
    return (
      <fieldset className="input_label">
        <label
        	className="label_text"
          htmlFor={this.props.htmlFor}
          label={this.props.label}
        >
          <input
            id={this.props.htmlFor}
            name={this.props.name || null}
            required={this.props.required || null}
            type='radio'
          />
          {this.props.label}
        </label>
      </fieldset>
    );
  }
});

// Component for select input
var Select = React.createClass ({
  render() {
    // Get all options from option prop
    const selectOptions = this.props.options.split(', ');

    // Generate list of options
    const selectOptionsList = selectOptions.map((selectOption, index) => {
      return <option className="Select-option" key={index} value={index}>{selectOption}</option>
    });

    return (
      <fieldset className="Select">
        <select
        	className="Select-control"
          defaultValue=''
          id={this.props.htmlFor}
          name={this.props.name || null}
          required={this.props.required || null}
        >
        	
          <option className="" value='' disabled>Country</option>

          {selectOptionsList}

        </select>
        <span className="Select-arrow" />
      </fieldset>
    );
  }
});

// Component for checkbox input
var Checkbox = React.createClass ({
  render() {
    return (
      <fieldset className="input_label">
        <label
        	className="label_text"
          htmlFor={this.props.htmlFor}
          label={this.props.label}
        >
          <input
            id={this.props.htmlFor}
            name={this.props.name || null}
            required={this.props.required || null}
            type='checkbox'
          />
          {this.props.label}
        </label>
      </fieldset>
    );
  }
});

// Component for input errors
var errorClass;
var cx = classNames.bind(errorClass);

var InputError = React.createClass({

  getInitialState: function(){
    return {
      message: 'Input is invalid'
    };
  },

  render: function(){ 
    var errorClass = cx({
      'error_container':   true,
      'visible':           this.props.visible,
      'invisible':         !this.props.visible
    });

    return (
      <div className={errorClass}>
        <span>{this.props.errorMessage}</span>
      </div>
    )
  }

})

// Component for icon image
var Icon = React.createClass({
  render: function() {
    switch(this.props.type) {

      case 'circle_error': 
        return (
          <svg viewBox="0 0 20 20">
          <path d="M10,0.982c4.973,0,9.018,4.046,9.018,9.018S14.973,19.018,10,19.018S0.982,14.973,0.982,10
            S5.027,0.982,10,0.982 M10,0C4.477,0,0,4.477,0,10c0,5.523,4.477,10,10,10s10-4.477,10-10C20,4.477,15.523,0,10,0L10,0z M9,5.703
            V5.441h2.5v0.262l-0.66,5.779H9.66L9,5.703z M9.44,12.951h1.621v1.491H9.44V12.951z"/>
          </svg>
        )

      case 'circle_tick': 
        return (
          <svg viewBox="0 0 23 23">
          <path d="M11.5,23C5.2,23,0,17.8,0,11.5S5.2,0,11.5,0S23,5.2,23,11.5S17.8,23,11.5,23z M11.5,1C5.7,1,1,5.7,1,11.5S5.7,22,11.5,22
            S22,17.3,22,11.5S17.3,1,11.5,1z M10.4,15.2l6.7-7c0.2-0.2,0.2-0.5,0-0.7c-0.2-0.2-0.5-0.2-0.7,0L10,14.2L7,11
            c-0.2-0.2-0.5-0.2-0.7,0c-0.2,0.2-0.2,0.5,0,0.7l3.4,3.5c0.1,0.1,0.2,0.1,0.3,0.1S10.3,15.3,10.4,15.2z"/>
          </svg>
        )

      case 'circle_tick_filled':
        return (
          <svg viewBox="0 0 20 20">
            <path fill="#4FB07F" d="M9.5,0C14.7,0,19,4.3,19,9.5S14.7,19,9.5,19S0,14.7,0,9.5S4.3,0,9.5,0z"/>
            <path fill="#FFFFFF" d="M8.7,12.9c-0.1,0-0.2,0-0.3-0.1l-2.4-2.5c-0.1-0.1-0.1-0.4,0-0.5c0.1-0.2,0.4-0.2,0.5,0L8.7,12l4.6-5
              c0.1-0.1,0.4-0.1,0.5,0c0.1,0.2,0.1,0.4,0,0.5L9,12.8C9,12.8,8.9,12.9,8.7,12.9C8.8,12.9,8.8,12.9,8.7,12.9z"/>
          </svg>
        )
 
    } 
  }
});

// Component for password validator
var validatorClass;
var cx = classNames.bind(validatorClass);

var PasswordValidator = React.createClass({
  getInitialState: function(){
    return {
      value: null,
      minCharacters: this.props.minCharacters,
      requireCapitals: this.props.requireCapitals,
      requireNumbers: this.props.requireNumbers,
      forbiddenWords: this.props.forbiddenWords,
      name: this.props.name
    };
  },

  render: function(){ 
    var validatorClass = cx({
      'password_validator':   true,
      'visible':              this.props.visible,
      'invisible':            !this.props.visible
    });

    var forbiddenWords = this.state.forbiddenWords.map(function(word, i) {
      return (
        <span key={i} className="bad_word">
          "{word}"
        </span>
      )
    })

    var validatorTitle;

    if(this.props.valid) {
      validatorTitle = 
        <h4 className="validator_title valid">
          {this.props.name} IS OK
        </h4>
    } else {
      validatorTitle = 
        <h4 className="validator_title invalid">
          {this.props.name} RULES
        </h4>
    }

    return (
      <div className={validatorClass}>
        <div className="validator_container">

          {validatorTitle}

          <ul className="rules_list">
        
            <li className={cx({'valid': this.props.validData.minChars})}> 
              <i className="icon_valid"> <Icon type="circle_tick_filled"/> </i>
              <i className="icon_invalid"> <Icon type="circle_error"/> </i>
              <span className="error_message">{this.state.minCharacters} characters minimum</span>
            </li>

            <li className={cx({'valid': this.props.validData.capitalLetters})}> 
              <i className="icon_valid"> <Icon type="circle_tick_filled"/> </i>
              <i className="icon_invalid"> <Icon type="circle_error"/> </i>
              <span className="error_message">Contains at least {this.state.requireCapitals} capital letter</span>
            </li>

            <li className={cx({'valid': this.props.validData.numbers})}> 
              <i className="icon_valid"> <Icon type="circle_tick_filled"/> </i>
              <i className="icon_invalid"> <Icon type="circle_error"/> </i>
              <span className="error_message">Contains at least {this.state.requireNumbers} number</span>
            </li>

            <li className={cx({'valid': this.props.validData.words})}> 
              <i className="icon_valid"> <Icon type="circle_tick_filled"/> </i>
              <i className="icon_invalid"> <Icon type="circle_error"/> </i>
              <span className="error_message">Can't be {forbiddenWords}</span>
            </li>

          </ul>
        </div>
      </div>
    )
  }
})

// Component for creating account screen
var CreateAccountScreen = React.createClass({
  getInitialState: function () {
    return {
    	firstLastName: null,
    	username: null,
      email: null,
      password: null,
      confirmPassword: null,
      date: null,
      phone: null,
      checkbox: null,
      forbiddenWords: ["password"]
    }
  },

  handlePasswordInput: function (event) {
    if(!_.isEmpty(this.state.confirmPassword)){
      this.refs.passwordConfirm.isValid();
    }
    this.refs.passwordConfirm.hideError();
    this.setState({
      password: event.target.value
    });
  },

  handleConfirmPasswordInput: function (event) {
    this.setState({
      confirmPassword: event.target.value
    });
  },

  saveAndContinue: function (e) {
    e.preventDefault();

    var canProceed = this.validateEmail(this.state.email) 
        
        && this.refs.password.isValid()
        && this.refs.passwordConfirm.isValid();

    if(canProceed) {
      var data = {
        email: this.state.email,
        firstLastName: this.state.firstLastName
      }
      alert('Thank You');
    } else {
      this.refs.firstLastName.isValid();
      this.refs.username.isValid();
      this.refs.email.isValid();
      this.refs.password.isValid();
      this.refs.passwordConfirm.isValid();
      this.refs.phone.isValid();
    }
  },

  isConfirmedPassword: function (event) {
    return (event == this.state.password)
  },

  handleNameInput: function(event) {
    this.setState({
      firstLastName: event.target.value
    })
  },

  validateName: function(event) {
  	var re = /[a-zA-Z0-9!#$%&'*+/=?^_`{|}~.-]{4,}/;
  	return re.test(event);
  },

  handleUsernameInput: function(event) {
    this.setState({
      username: event.target.value
    })
  },

  validateUsername: function (event) {
  	var re = /^[0-9a-zA-Z\_]{3,}$/;
  	return re.test(event);
  },

  handleEmailInput: function(event){
    this.setState({
      email: event.target.value
    });
  },

  validateEmail: function (event) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(event);
  },

  handlePhoneNumber: function(event) {
  	this.setState({
  		phone: event.target.value
  	});
  },

  validatePhone: function(event) {
  	var re = /^\d{8,}/;
  	return re.test(event);
  },

  handleDate: function(event) {
  	this.setState({
  		date: event.target.value
  	});
  },

  isEmpty: function (value) {
    return !_.isEmpty(value);
  },


  render: function() {
    return (
      <div className="create_account_screen">

        <div className="create_account_form">
          <h1>Register your account</h1>
          <p></p>
          <form onSubmit={this.saveAndContinue}>

          	<Input 
              text="First Name, Last Name" 
              ref="firstLastName"
              type="text"
              defaultValue={this.state.firstLastName}
              validate={this.validateName}
              value={this.state.firstLastName}
              onChange={this.handleNameInput}
              errorMessage="Your name must be longer" 
              emptyMessage="Please enter your name"
            /> 

            <Input 
              text="Username" 
              ref="username"
              type="text"
              defaultValue={this.state.username}
              validate={this.validateUsername}
              value={this.state.username}
              onChange={this.handleUsernameInput} 
              errorMessage="You can only use numbers, letters and _"
              emptyMessage="Username can't be empty"
            /> 

            <Input 
              text="E-mail" 
              ref="email"
              type="text"
              defaultValue={this.state.email} 
              validate={this.validateEmail}
              value={this.state.email}
              onChange={this.handleEmailInput} 
              errorMessage="Email is invalid"
              emptyMessage="Email can't be empty"
              errorVisible={this.state.showEmailError}
            />

            <Input 
              text="Password" 
              type="password"
              ref="password"
              validator="true"
              minCharacters="8"
              requireCapitals="1"
              requireNumbers="1"
              forbiddenWords={this.state.forbiddenWords}
              value={this.state.password}
              emptyMessage="Password is invalid"
              onChange={this.handlePasswordInput} 
            /> 

            <Input 
              text="Confirm password" 
              ref="passwordConfirm"
              type="password"
              validate={this.isConfirmedPassword}
              value={this.state.confirmPassword}
              onChange={this.handleConfirmPasswordInput} 
              emptyMessage="Please confirm your password"
              errorMessage="Passwords don't match"
            /> 

            <Input 
              text="Date of birth" 
              ref="date"
              type="date"
              value={this.state.date}
              defaultValue={this.state.date}
              onChange={this.handleDate} 
            /> 

						<Input 
              text="Phone number" 
              ref="phone"
              type="text"
              defaultValue={this.state.phone}
              validate={this.validatePhone}
              value={this.state.phone}
              onChange={this.handlePhoneInput} 
              errorMessage="You can only use numbers. Must be longer than 8 numbers"
              emptyMessage="Phone number can't be empty"
            />        

            <h1>Gender:</h1>
            <Radio
          		hasLabel='true'
          		htmlFor='male'
          		label='Male'
          		name='radio'
       		 	/>    

       		 	<Radio
          		hasLabel='true'
          		htmlFor='female'
          		label='Female'
          		name='radio'
       		 	/>    

       		 	<Select
		          hasLabel='true'
		          htmlFor='country'
		          label='Country'
		          options='USA, France, Italy, England, Ukraine'
		        />

		        <Checkbox
		          hasLabel='true'
		          htmlFor='checkbox'
		          label='Registration agreement'
		          errorMessage=""
              emptyMessage=""
		        />

            <button 
              type="submit" 
              className="button button_wide">
              REGISTER ACCOUNT
            </button>

          </form>

        </div>
      </div>
    );
  }   
});
    
// Component for our application
var App = React.createClass({
  render: function() {
    return (
      <div className="application_wrapper">

        <div className="application_routeHandler">
            <CreateAccountScreen/>
        </div>
        
      </div>
    );
  } 
});
  
// Render App component
ReactDOM.render(<App/>, document.getElementById('form'));